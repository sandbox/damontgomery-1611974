<?php
/**
 * @file
 * MailSystemInterface for disabling mail on dev servers
 *
 * To enable, save a variable in settings.php (or otherwise) whose value
 * can be as simple as:
 *
 * $conf['mail_system'] = array(
 *   'default-system' => 'DisableMail',
 *);
 */

class DisableMail extends DefaultMailSystem {
  /**
   * Save an e-mail message to a file, using Drupal variables and default settings.
   *
   * @see http://php.net/manual/en/function.mail.php
   * @see drupal_mail()
   *
   * @param $message
   *   A message array, as described in hook_mail_alter().
   * @return
   *   TRUE to pretend the mail was sent.
   */
  public function mail(array $message) {
    return TRUE;
  }
}
?>
